/**
RADU DBG 
*/

#ifndef RADU_DEV_DEBUG_H
#define RADU_DEV_DEBUG_H
//#define LLD_DBG_ENABLED
#define ADC_DBG_ENABLED
//#define SPI_LLD_DBG_ENABLED
#define HLD_DBG_ENABLED
#define PROCESS_DBG_ENABLED


#ifdef HLD_DBG_ENABLED

	#define HDBG(str , var) printk(KERN_INFO str, var )
#else
	#define HDBG(str , var)
#endif

#ifdef LLD_DBG_ENABLED

	#define LDBG(str , var) printk(KERN_INFO str, var )
#else

	#define LDBG(str , var)  
#endif

#ifdef SPI_LLD_DBG_ENABLED

	#define SLDBG(str , var) printk(KERN_INFO str, var )
#else

	#define SLDBG(str , var)  
#endif

#ifdef ADC_DBG_ENABLED

	#define ALDBG(str , var) printk(KERN_INFO str, var )
#else

	#define ALDBG(str , var)  
#endif

#ifdef PROCESS_DBG_ENABLED

	#define PDBG(str , var) printk(KERN_INFO str, var )
#else

	#define PDBG(str , var)  
#endif

#endif