#include <linux/types.h> 
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/workqueue.h>

#include "radu_dev_adc_LLD.h"
#include "radu_dev_debug.h"


struct workqueue_struct  * p_my_workqueue;

void DataProcess(unsigned long data_pointer);
void DataProcessW(struct work_struct *work);

DECLARE_TASKLET(process_tasklet, DataProcess, (unsigned long)&adc_buff);

DECLARE_WORK(adc_work, DataProcessW);


void DataProcess(unsigned long data_pointer){

uint16_t * pl_adc_buff = ((adc_buff_t *)data_pointer)->read_buffer;
uint32_t   med_val = 0;
int i,j;

PDBG("TASKLET enter %08x\n",(uint32_t)pl_adc_buff);

for(j=0 ; j < 100 ;j++){ /*Dummy for loop just to make the tasklet last longer*/
	med_val = 0;

	for(i=0; i < ADC_MED_SIZE ; i++){
		med_val += pl_adc_buff[i];
	}
	med_val /= ADC_MED_SIZE; 

}
adc_buff.med_value = (uint16_t)med_val;
adc_buff.flag=1;
PDBG("TASKLET exit med %08x \n",(uint32_t)med_val);

}

void DataProcessW(struct work_struct *work){


uint32_t   med_val = 0;
int i,j;

PDBG("WORKq enter %08x\n",0);

for(j=0 ; j < 1000 ;j++){ /*Dummy for loop just to make the work last longer*/
	med_val = 0;

	for(i=0; i < ADC_MED_SIZE ; i++){
		med_val += i/(j+1); /*Dummy computation*/
	}
	med_val /= ADC_MED_SIZE; 
}

PDBG("WORKq exit med %08x \n",(uint32_t)med_val);

}



