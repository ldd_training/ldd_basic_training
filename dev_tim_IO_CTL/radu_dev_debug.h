/**
RADU DBG 
*/

#ifndef RADU_DEV_DEBUG_H
#define RADU_DEV_DEBUG_H
//#define LLD_DBG_ENABLED
#define HLD_DBG_ENABLED


#ifdef HLD_DBG_ENABLED

	#define HDBG(str , var) printk(KERN_INFO str, var )
#else
	#define HDBG(str , var)
#endif

#ifdef LLD_DBG_ENABLED

	#define LDBG(str , var) printk(KERN_INFO str, var )
#else

	#define LDBG(str , var)  
#endif




#endif