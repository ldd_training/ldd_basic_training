/**
RADU DEV ADC 
*/
#include <asm/io.h>
#include <linux/module.h>
#include <linux/types.h> 
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/workqueue.h>

#include "radu_process_data.h"
#include "radu_dev_gpio_LLD.h"
#include "radu_dev_spi_LLD.h"
#include "radu_dev_adc_LLD.h"
#include "radu_dev.h"
#include "radu_dev_debug.h"

#define ADC_CH_MASK 0x07
#define ADC_SGL 0x08
#define ADC_START 0x10

adc_buff_t adc_buff;

irqreturn_t SpiAdcIrq(int irq, void *dev_id){

ALDBG( "Irq!%d\n",irq);
//ALDBG( "I=%d\n",adc_buff.index);
switch(adc_buff.index)
{
	case 0:
		GpioSetState( p_gpio_base , SPI_SPI0_CE0, 0u);
		
		SpiSendIrq( p_spi_base ,adc_buff.data[0], 0);
		adc_buff.index++;
		break;
	case 1:
		SpiSendIrq( p_spi_base ,adc_buff.data[1], 1);
		adc_buff.index++;
		break;
	case 2:
		SpiSendIrq( p_spi_base ,adc_buff.data[2], 1);
		adc_buff.index++;
		break;
	case 3:
		GpioSetState( p_gpio_base , SPI_SPI0_CE0, 1u);
		adc_buff.index=0;
		
		/*READ ADC RESULT*/
		{
		uint16_t temp = 0;
		SpiRead(p_spi_base); // dummy read only HIGH Z
		temp  = SpiRead(p_spi_base);
		temp &= (uint32_t)0x7f; // ERASE THE VALUE THAT IS IN HIGH Z
		temp = temp << 8;
		temp |= SpiRead(p_spi_base);
		adc_buff.read_buffer[adc_buff.buff_index++]= temp >> 4; //ONLY four matter!
		
		ALDBG( "buff_index=%d\n",adc_buff.buff_index);
		}
		
		/*GO dummy workq*/
		if(adc_buff.buff_index ==(ADC_MED_SIZE - 4)){
			ALDBG( "WORK_SCHEDULED!!! index=%d\n",adc_buff.buff_index);
			queue_work(p_my_workqueue,(struct work_struct *)&adc_work);
		}
		
		/*GO tasklet*/
		if(adc_buff.buff_index == ADC_MED_SIZE){
			ALDBG( "TASKLET_SCHEDULED!!! index=%d\n",adc_buff.buff_index);
			tasklet_hi_schedule(&process_tasklet);
		}
		
		/*finish the aqsition*/
		if(adc_buff.buff_index >= ADC_BUF_SIZE){
			adc_buff.buff_index = 0;
			SpiClearIrq(p_spi_base); // disable interrupts
			  
		}else{ /*start a new ADC conversion*/
			GpioSetState( p_gpio_base , SPI_SPI0_CE0, 0u);
			
			SpiSendIrq( p_spi_base ,adc_buff.data[0], 0);
			adc_buff.index++;
		}
		
		
		
		break;
}

//ALDBG( "F=%d\n",adc_buff.flag);
return IRQ_HANDLED;
}
int AdcInit(uint16_t * buffer_add){
  GpioSetFunction(p_gpio_base , SPI_SPI0_MOSI , GPIO_ALT0);
  GpioSetFunction(p_gpio_base , SPI_SPI0_MISO , GPIO_ALT0);
  GpioSetFunction(p_gpio_base , SPI_SPI0_SCLK , GPIO_ALT0);
  GpioSetFunction(p_gpio_base , SPI_SPI0_CE0 , GPIO_OUTPUT); 
  GpioSetState( p_gpio_base , SPI_SPI0_CE0, 1u);
  SpiInitFunction(p_spi_base, 2048);
  adc_buff.data[1] =0xAA;
  adc_buff.data[2] =0xAA;
  adc_buff.index=0;
  adc_buff.med_value=0;
  adc_buff.read_buffer = buffer_add;
  adc_buff.flag=0;
  adc_buff.buff_index=0;
  return 0;
}

uint32_t AdcChRead(uint32_t vl_chnr){

adc_buff.data[0]=(vl_chnr & ADC_CH_MASK) | ADC_SGL | ADC_START;
	
	 SpiStartIrq( p_spi_base , 0);
	 while((adc_buff.flag == 0)); /*!!!!!!!*/
	 
	 adc_buff.flag=0;
	 return adc_buff.med_value;
}

int AdcDeinit(void){
  GpioSetFunction(p_gpio_base , SPI_SPI0_MOSI , GPIO_INPUT);
  GpioSetFunction(p_gpio_base , SPI_SPI0_MISO , GPIO_INPUT);
  GpioSetFunction(p_gpio_base , SPI_SPI0_SCLK , GPIO_INPUT);
  GpioSetFunction(p_gpio_base , SPI_SPI0_CE0 , GPIO_INPUT);
  SpiDeinitFunction(p_spi_base);
   return 0;
}








