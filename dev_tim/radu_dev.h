/**
*
*RADU_DEV_types_declaration
*
*/
#include <linux/types.h>
#include <linux/timer.h>

typedef struct {

timer_list timer;
uint32_t state;
uint32_t gpio_nr;

} t_tim_data;