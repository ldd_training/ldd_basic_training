#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <linux/mutex.h>
#include <asm/uaccess.h>

#define	DRVNAME		"alloc_test"

/*Structure that holds all module data

*/

typedef struct _AllocTest{
    u32 vBytesToAlloc;
    u32 kBytesToAlloc;
    void * vstart_adress;
    void * kstart_adress;
    char * vname;
    char * kname;
    struct mutex lock;
    u8  LastAtempt;
} t_AllocTest , *tp_AllocTest; 

static tp_AllocTest gp_AllocTest = NULL;

/********************************VMALLOC_TEST*****************************/
/* 
read callback
*/
static int vmalloc_procread(char *buf, char **start, off_t offset,
                   int count, int *eof, void *data)
{
	int n;
    tp_AllocTest lp_drv_data = (tp_AllocTest)data;
    mutex_lock (&lp_drv_data->lock);
    if(lp_drv_data->vstart_adress == NULL){
	n = sprintf (buf, "vmalloc Failed to allocate %u bytes (%u Kb, %u MB) \n", 
			lp_drv_data->vBytesToAlloc, lp_drv_data->vBytesToAlloc/1024, lp_drv_data->vBytesToAlloc/(1024*1024));
    } else {
	n = sprintf (buf, "vmalloc Allocated with succes %u bytes (%u Kb, %u MB) \n", 
			lp_drv_data->vBytesToAlloc, lp_drv_data->vBytesToAlloc/1024, lp_drv_data->vBytesToAlloc/(1024*1024));
    }
    mutex_unlock (&lp_drv_data->lock);
    
    *eof = 1;
	return n;
}

/*
write callback:
Userspace expected to write to this proc entry, the number of bytes to
attempt to allocate.
*/
static int vmalloc_procwrite(struct file *file, const char __user *buffer,
                           unsigned long count, void *data)
{
	tp_AllocTest lp_drv_data = (tp_AllocTest)data;
	int sz = 18; /* # of digits (=bytes written in) */ 
    int status = count;
	char kbuf[sz+1];

//MSG("count=%d\n", count);
	mutex_lock (&lp_drv_data->lock);

	if (count > sz) {
		printk(KERN_INFO"%s: writing a number > %d digits is invalid\n", DRVNAME, sz);
		status = -EINVAL;
		goto out_inval;
	}
    /*return value is the amount of memory still to be copied.*/
	if (copy_from_user (kbuf, buffer, sz)) {
		printk("%s: copy_from_user() failed!\n", DRVNAME);
		status = -EIO;
		goto out_inval;
	}
	kbuf[sz+1]='\0';

	lp_drv_data->vBytesToAlloc = simple_strtoul(kbuf, NULL, 10);
	if (0 == lp_drv_data->vBytesToAlloc) {
		printk("%s: writing invalid numeric value \"%.*s\", aborting...\n", DRVNAME, (int)count, kbuf);
		status = -EINVAL;
		goto out_inval;
	}
    

	lp_drv_data->vstart_adress = vmalloc (lp_drv_data->vBytesToAlloc);
	if (!lp_drv_data->vstart_adress) {
		printk (KERN_INFO "%s: vmalloc of %u bytes FAILED!\n", DRVNAME, lp_drv_data->vBytesToAlloc);
		status = -ENOMEM;
		goto out_inval;
	}
	printk (KERN_INFO "Successfully allocated via vmalloc %u bytes (%u Kb, %u MB) now to location 0x%08x (will vfree..)\n", 
			lp_drv_data->vBytesToAlloc, lp_drv_data->vBytesToAlloc/1024, lp_drv_data->vBytesToAlloc/(1024*1024), (u32)lp_drv_data->vstart_adress);


	vfree (lp_drv_data->vstart_adress);

out_inval:
	mutex_unlock (&lp_drv_data->lock);
	return status;
}


//---------------------------- KMALLOC_TEST ------------------------------------------
/* 
kmalloc_test Read callback: just display the last # bytes attempting to allocate...
*/
static int kmalloc_procread(char *buf, char **start, off_t offset,
                   int count, int *eof, void *data)
{
	int n;
    tp_AllocTest lp_drv_data = (tp_AllocTest)data;
    mutex_lock (&lp_drv_data->lock);
    if(lp_drv_data->kstart_adress == NULL){
	n = sprintf (buf, "kmalloc Failed to allocate %u bytes (%u Kb, %u MB) \n", 
			lp_drv_data->kBytesToAlloc, lp_drv_data->kBytesToAlloc/1024, lp_drv_data->kBytesToAlloc/(1024*1024));
    } else {
	n = sprintf (buf, "kmalloc Allocated with succes %u bytes (%u Kb, %u MB) \n", 
			lp_drv_data->kBytesToAlloc, lp_drv_data->kBytesToAlloc/1024, lp_drv_data->kBytesToAlloc/(1024*1024));
    }
    mutex_unlock (&lp_drv_data->lock);
    
    *eof = 1;
	return n;
}

/*
kmalloc_test Write callback:
Userspace expected to write to this proc entry, the number of bytes to
attempt to allocate.
*/
static int kmalloc_procwrite(struct file *file, const char __user *buffer,
                           unsigned long count, void *data)
{
	tp_AllocTest lp_drv_data = (tp_AllocTest)data;
	int sz=18; /* # of digits (=bytes written in) */ 
    int status=count;
	char kbuf[sz+1];

//MSG("count=%d\n", count);
	mutex_lock (&lp_drv_data->lock);

	if (count > sz) {
		printk(KERN_INFO"%s: writing a number > %d digits is invalid\n", DRVNAME, sz);
		status = -EINVAL;
		goto out_inval;
	}
    /*return value is the amount of memory still to be copied.*/
	if (copy_from_user (kbuf, buffer, sz)) {
		printk("%s: copy_from_user() failed!\n", DRVNAME);
		status = -EIO;
		goto out_inval;
	}
	kbuf[sz+1]='\0';

	lp_drv_data->kBytesToAlloc = simple_strtoul(kbuf, NULL, 10);
	if (0 == lp_drv_data->kBytesToAlloc) {
		printk("%s: writing invalid numeric value \"%.*s\", aborting...\n", DRVNAME, (int)count, kbuf);
		status = -EINVAL;
		goto out_inval;
	}
    

	lp_drv_data->kstart_adress = kmalloc (lp_drv_data->kBytesToAlloc , GFP_KERNEL);
	if (!lp_drv_data->kstart_adress) {
		printk (KERN_INFO "%s: kmalloc of %u bytes FAILED!\n", DRVNAME, lp_drv_data->kBytesToAlloc);
		status = -ENOMEM;
		goto out_inval;
	}
	printk (KERN_INFO "Successfully allocated via vmalloc %u bytes (%u Kb, %u MB) now to location 0x%08x (will vfree..)\n", 
			lp_drv_data->kBytesToAlloc, lp_drv_data->kBytesToAlloc/1024, lp_drv_data->kBytesToAlloc/(1024*1024), (u32)lp_drv_data->kstart_adress);


	kfree (lp_drv_data->kstart_adress);

out_inval:
	mutex_unlock (&lp_drv_data->lock);
	return status;
}

static struct proc_dir_entry *res;
static int __init alloc_test_init(void)
{
	int status=0;

	//-------- Alloc & init global context struct
    /*kzalloc — allocate memory. The memory is set to zero. */
	gp_AllocTest = kzalloc (sizeof(t_AllocTest), GFP_KERNEL);
	if (!gp_AllocTest) {
		printk(KERN_INFO"%s: kzalloc 1 failed, aborting..\n", DRVNAME);
		status = -ENOMEM;
		goto out_k1_fail;
	}

	gp_AllocTest->kname = kmalloc (128, GFP_KERNEL);
	if (!gp_AllocTest->kname) {
		printk(KERN_INFO"%s: kmalloc 2 failed, aborting..\n", DRVNAME);
		status = -ENOMEM;
		goto out_k2_fail;
	}
	strncpy (gp_AllocTest->kname, "driver/kmalloc_test", 128);

	gp_AllocTest->vname = kmalloc (128, GFP_KERNEL);
	if (!gp_AllocTest->vname) {
		printk(KERN_INFO"%s: kmalloc 3 failed, aborting..\n", DRVNAME);
		status = -ENOMEM;
		goto out_k3_fail;
	}
	strncpy (gp_AllocTest->vname, "driver/vmalloc_test", 128);
	mutex_init (&gp_AllocTest->lock);

	//-------- Setup proc entry points
	res = create_proc_entry (gp_AllocTest->kname, 0644, /* mode */ NULL);
	if (!res) {
		printk(KERN_INFO"%s: Entry %s creation failure, aborting..\n", DRVNAME, gp_AllocTest->kname);
		status = -ENOMEM;
		goto out_p1_fail;
	}
	res->read_proc = kmalloc_procread;
	res->write_proc = kmalloc_procwrite;
	res->data = gp_AllocTest;

	res = create_proc_entry (gp_AllocTest->vname, 0644, /* mode */ NULL);
	if (!res) {
		printk(KERN_INFO"%s: Entry %s creation failure, aborting..\n", DRVNAME, gp_AllocTest->vname);
		status = -ENOMEM;
		goto out_p2_fail;
	}
	res->read_proc = vmalloc_procread;
	res->write_proc = vmalloc_procwrite;
	res->data = gp_AllocTest;

	printk(KERN_INFO"Loaded ok.\n");
	return 0;

out_p2_fail:
	remove_proc_entry (gp_AllocTest->kname, NULL);
out_p1_fail:
	kfree (gp_AllocTest->vname);
out_k3_fail:
	kfree (gp_AllocTest->kname);
out_k2_fail:
	kfree (gp_AllocTest);
out_k1_fail:
	return status;
}

static void __exit alloc_test_cleanup(void)
{
	remove_proc_entry (gp_AllocTest->vname, NULL);
	remove_proc_entry (gp_AllocTest->kname, NULL);
	kfree (gp_AllocTest->vname);
	kfree (gp_AllocTest->kname);
	kfree (gp_AllocTest);
	printk(KERN_INFO"Removed.\n");
}

/*info */
module_init(alloc_test_init);
module_exit(alloc_test_cleanup);
MODULE_AUTHOR("Jean de la Craiova");
MODULE_DESCRIPTION("TEST kmalloc / vmalloc limits");
MODULE_LICENSE("GPL");


