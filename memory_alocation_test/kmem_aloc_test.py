#!/usr/bin/python


ENOMEM= -12;      # Out of memory 
PAGE_SIZE = 4096;
KMALLOC_PROC_FILE = "/proc/driver/kmalloc_test";

print "TEST\n"
# Open a file
fo = open(KMALLOC_PROC_FILE, "r+",0);
bytes =0;
status = 0;
while(status != ENOMEM):
    bytes += PAGE_SIZE;
    status = fo.write(str(bytes));
    fo.seek(0);
    print fo.read();
    
fo.close();		



