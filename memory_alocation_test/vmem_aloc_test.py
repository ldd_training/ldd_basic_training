#!/usr/bin/python
import time
import fcntl #This module performs file control and I/O control on file descriptors

ENOMEM= -12;      # Out of memory 
PAGE_SIZE = 4096;
VMALLOC_PROC_FILE = "/proc/driver/vmalloc_test";

print "TEST\n"
# Open a file
fo = open(VMALLOC_PROC_FILE, "r+",0);
bytes =0;
status = 0;
while(status != ENOMEM):
    bytes += 400*PAGE_SIZE;
    status = fo.write(str(bytes));
    fo.seek(0);
    print fo.read();
    
fo.close();		



