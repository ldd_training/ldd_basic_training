/* ofd.c � Our First Driver code */
#include <linux/module.h> /*module_init() and module_exit(), which are defined in the kernel header module.h.*/
#include <linux/version.h> /*Additionally, version.h is included for the module version to be compatible with the kernel*/
#include <linux/kernel.h>/*Note that there is no stdio.h (a user-space header); instead, we use the analogous kernel.h (a kernel space header). printk() is the equivalent of printf().*/
#include <linux/types.h> /*dev_t defined in kernel header linux/types.h*/
#include <linux/kdev_t.h> /*MAJOR MINOR MKDEV Macros (defined in kernel header linux/kdev_t.h)*/
#include <linux/fs.h> /*alloc_chrdev_region(dev_t *first, unsigned int firstminor, unsigned int cnt, char *name);*/
#include <linux/device.h>/* driver is concerned, the appropriate /sys entries need to be populated using the Linux device model APIs declared in <linux/device.h>.*/
#include <linux/cdev.h>/*oth cdev_init() and cdev_add() are declared in <linux/cdev.h>*/
#include <asm-generic/uaccess.h>
#include <asm/io.h>
#include <linux/proc_fs.h> /*All modules that work with /proc should include*/
#include <linux/timer.h>
#include <linux/jiffies.h>
#include "radu_dev_LLD.h"
//#include "radu_dev.h"


/*DRIVER INTERNAL DEFINES*/
#define BUFF_SIZE 256
#define DELAY_100MS  HZ/100
#define TOTAL_GPIOS 3
/*DRIVER INTERNAL DEFINES END*/
/*DRIVER TYPE DEFINES*/
typedef struct {

struct timer_list timer;
uint32_t state;
uint32_t gpio_nr;
uint32_t delay;

} t_tim_data;
/*DRIVER DEFINES END*/
/*DRIVER INTERNAL GLOBAL VARIABLES*/
static dev_t radu_dev; // Global variable for devicec driver 
static struct cdev c_dev; // Global variable for the character device structure
static struct class * dev_class;  // Global variable for the device class
//static char v_InternalBuffer[BUFF_SIZE];
//static int v_InternalBufferSize = 0;
static void __iomem * p_gpio_base = NULL;
static t_tim_data v_device_timer[TOTAL_GPIOS];
static t_tim_data * p_device_timer = &v_device_timer[0];
//static v_gpio_started = 0;

/*TIMER HANDLER*/
void tim_handler(unsigned long arg){

t_tim_data * pl_device_timer = (t_tim_data *)arg;

pl_device_timer->timer.expires += pl_device_timer->delay;
add_timer(&pl_device_timer->timer);

if(pl_device_timer->state){
GpioSetState(p_gpio_base, pl_device_timer->gpio_nr, 1);
}else{
GpioSetState(p_gpio_base, pl_device_timer->gpio_nr, 0);
}

pl_device_timer->state ^= 1;

}
/*TIMER HANDLER END*/
/*DRIVER INTERNAL GLOBAL VARIABLES END*/
/* return 0 == succes*/
static int radu_open(struct inode *i, struct file *f)
{
  printk(KERN_INFO "char Driver: open()\n");
  return 0;
}
/* return 0 == succes*/
static int radu_close(struct inode *i, struct file *f)
{
  printk(KERN_INFO "char Driver: close()\n");
  return 0;
}
/*returning a negative number would be a usual error*/
/*return bytes read*/
static ssize_t radu_read(struct file *f, char __user *buf, size_t count, loff_t *possition)
{


  printk(KERN_INFO " char Driver: IO_GPIO_GPFSEL0 %x \n",(unsigned int) ioread32(p_gpio_base + IO_GPIO_GPFSEL0_OF) );
  printk(KERN_INFO " char Driver: IO_GPIO_GPFSEL1 %x \n",(unsigned int) ioread32(p_gpio_base + IO_GPIO_GPFSEL1_OF ) );
  printk(KERN_INFO " char Driver: IO_GPIO_GPSET0_OF %x \n",(unsigned int) ioread32(p_gpio_base + IO_GPIO_GPSET0_OF ) );
  printk(KERN_INFO " char Driver: IO_GPIO_GPFSEL3 %x \n",(unsigned int) ioread32(p_gpio_base + IO_GPIO_GPFSEL3_OF ) );
  printk(KERN_INFO " char Driver: IO_GPIO_GPFSEL4 %x \n",(unsigned int) ioread32(p_gpio_base + IO_GPIO_GPFSEL4_OF ) );
  printk(KERN_INFO " char Driver: IO_GPIO_GPFSEL5 %x \n",(unsigned int) ioread32(p_gpio_base + IO_GPIO_GPFSEL5_OF ) );
  printk(KERN_INFO "radu-driver: Device file is read at offset = %i, read bytes count = %u", (int)*possition  , (unsigned int)count );
    
		/*if (copy_to_user(buf, &v_InternalBuffer,v_InternalBufferSize ) != 0){
			return -EFAULT;
		}
		else if(*possition >= 1){
			return 0;
		}
		else {
			*possition += 1;
			return v_InternalBufferSize;
		}
     */
}


/*returning a negative number would be a usual error*/
/*return bytes written*/
/*message format 1 char ticker  2 char gpio nr  1 char on/off 3 char delay ms*10 */
 static ssize_t radu_write(struct file *f, const char __user *buf, size_t count, loff_t *possition)
{
  char c[10];
  uint32_t vl_gpio_nr = 0;
  uint32_t vl_ticker_nr = 0;
  uint32_t vl_delay = 0;
  t_tim_data * pl_device_timer;
  unsigned long j = jiffies;
  
  printk(KERN_INFO " char Driver: write()\n");
	if (copy_from_user(&c, buf, (count > 10 ? 10 : count)) != 0){
		return -EFAULT;
	}
	else
	{			
		vl_ticker_nr = ((uint32_t)c[0] -48);
		vl_gpio_nr = ((uint32_t)c[1] -48)*10  + ((uint32_t)c[2] -48);
		vl_delay =  ((uint32_t)c[4] -48)*100  + ((uint32_t)c[5] -48)*10 + ((uint32_t)c[6] -48);
		
		if(c[3] == '1'){
			if( vl_ticker_nr < TOTAL_GPIOS){
				pl_device_timer = &v_device_timer[vl_ticker_nr];
				if(pl_device_timer->delay != 0){
					del_timer(&pl_device_timer->timer);
				}
				GpioSetFunction(p_gpio_base, vl_gpio_nr , GPIO_OUTPUT);
				pl_device_timer->gpio_nr = vl_gpio_nr;
				pl_device_timer->state = 0;
				/*register timer*/
				init_timer(&pl_device_timer->timer);
				pl_device_timer->timer.data = (unsigned long)pl_device_timer;
				pl_device_timer->timer.function = tim_handler;
				pl_device_timer->timer.expires = j + vl_delay;
				pl_device_timer->delay = vl_delay;
				add_timer(&pl_device_timer->timer);
			}
			/*fops related*/
			possition += count;
			return count;
		}else if(c[3] == '0'){
			if( vl_ticker_nr < TOTAL_GPIOS){
				pl_device_timer = &v_device_timer[vl_ticker_nr];
				GpioSetFunction(p_gpio_base, pl_device_timer->gpio_nr , GPIO_INPUT);
				if(pl_device_timer->delay){
				del_timer(&pl_device_timer->timer);}
				(pl_device_timer)->state = 0;
				(pl_device_timer)->gpio_nr = 0;
				(pl_device_timer)->delay = 0;
			}
			/*fops related*/
			possition += count;
			return count;
		}

	possition += count;
	return count;
	}
}


/*The ioctl system call offers a way to issue device-specific commands*/
static long radu_ioctl(struct file *fo, unsigned int comm , unsigned long arg){

printk(KERN_INFO"IOCTL: COMM%d ARG%d \n",comm,arg);

return 0;
}
/* fill in a file operations structure with the desired file operations*/
static struct file_operations radu_fops =
{
  .owner = THIS_MODULE,
  .open = radu_open,
  .release = radu_close,
  .read = radu_read,
  .write = radu_write,
  .unlocked_ioctl = radu_ioctl
};

/*PROC INTERFACE*/
int radu_read_procmem(char *buf, char **start, off_t offset, int count, int *eof, void *data){
	int len = 0;
	ReportGpioHwValues( buf ,&len,p_gpio_base);
	*eof = 1;
	return len;
}

/*PROC INTERFACE END */
static int __init radu_init(void) /* Constructor */
{
int i;
    
	/*Like vmalloc, ioremap builds new page tables; unlike vmalloc, however, it doesn�t
	 actually allocate any memory. The return value of ioremap is a special virtual address
     that can be used to access the specified physical address range*/ 
	if ((p_gpio_base = ioremap(IO_GPIO_BASE , IO_GPIO_SIZE)) == NULL){
        printk(KERN_ERR "Mapping GPIO failed\n");
		goto fault_0;
    }
	/*The second API dynamically figures out a free major number, and registers the cnt number
		of device file numbers starting from <the free major, firstminor>,*/
    if(alloc_chrdev_region(&radu_dev , 0 ,1,"radu_tim_char_driver") < 0){
		printk(KERN_ERR "Char driver region alocation failed\n");
		goto fault_1;
    }
	/*/sys entries need to be populated using the Linux device model APIs declared*/
	/*The rest should be handled by udev*/
	if ((dev_class = class_create(THIS_MODULE, "chardrv")) == NULL){	
		printk(KERN_ERR "Device class failed\n");
		goto fault_2;
	}
	/*Then, the device info (<major, minor>) under this class is populated by:*/
	if (device_create(dev_class, NULL, radu_dev, NULL, "my_tim_ioctl_gpio") == NULL){
		printk(KERN_ERR "Device create failed\n");
		goto fault_3;
	}
	/*initialise the character device structure*/
	cdev_init(&c_dev, &radu_fops);
	/* hand this structure to the VFS using the call cdev_add().*/
	if (cdev_add(&c_dev, radu_dev, 1) == -1)
	{
		goto fault_4;
	}
	if(create_proc_read_entry("my_tim_gpio_stat", 0 /* default mode */,NULL /* parent dir */
	, radu_read_procmem, NULL /* client data */) == NULL){
		goto fault_4;
	}
	
		
	for(i = 0;i < TOTAL_GPIOS;i++){
	
	init_timer(&(p_device_timer+i)->timer);
	GpioSetFunction(p_gpio_base, (p_device_timer+i)->gpio_nr , GPIO_INPUT);
	(p_device_timer+i)->state = 0;
	(p_device_timer+i)->gpio_nr = 0;
	(p_device_timer+i)->delay = 0;
	}
	printk(KERN_INFO "++++++++++++++++++++++++\n");
	printk(KERN_INFO "Radu  tim device registered\n");
	printk(KERN_INFO "++++++++++++++++++++++++\n");
    return 0;
	fault_4 : device_destroy(dev_class, radu_dev);
	fault_3 : class_destroy(dev_class);
	fault_2 : unregister_chrdev_region(radu_dev, 1);
	fault_1 : iounmap(p_gpio_base);
	fault_0 : 
	
	return -1;
}
static void __exit radu_exit(void) /* Destructor */
{
	int i;
	for(i=0;i<TOTAL_GPIOS;i++){
		GpioSetFunction(p_gpio_base, (p_device_timer+i)->gpio_nr , GPIO_INPUT);
		if((p_device_timer+i)->delay){
			del_timer(&(p_device_timer+i)->timer);
			(p_device_timer+i)->state = 0;
			(p_device_timer+i)->gpio_nr = 0;
			(p_device_timer+i)->delay = 0;
		}
	}
	remove_proc_entry("my_tim_gpio_stat", NULL /* parent dir */);
	device_destroy(dev_class, radu_dev);
    class_destroy(dev_class);
	unregister_chrdev_region(radu_dev, 1);
	iounmap(p_gpio_base);
	printk(KERN_INFO "++++++++++++++++++++++++\n");
    printk(KERN_INFO "Radu tim device unregistered\n");
	printk(KERN_INFO "++++++++++++++++++++++++\n");
}
 
module_init(radu_init);
module_exit(radu_exit);
 
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Radu Radu");
MODULE_DESCRIPTION("Radu's First Driver");
