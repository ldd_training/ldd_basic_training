/**
RADU DEV LLD
*/
#include <asm/io.h>
#include <linux/module.h>
#include <linux/types.h> 
#include <linux/kernel.h>
#include "radu_dev_LLD.h"
#include "radu_dev_debug.h"



void GpioSetFunction(void __iomem * vl_gpio_base , uint32_t gpio_nr , uint32_t func){
uint32_t vl_address;
uint32_t vl_reg;

vl_address = (uint32_t)vl_gpio_base + (uint32_t)IO_GPIO_GPFSEL0_OF;
vl_address += ((gpio_nr / 10 ) << 2);

LDBG("GPFSEL ADDRESS %x \n",vl_address);

vl_reg = ioread32((void *)vl_address);
LDBG("GPFSEL VALUE BEFORE %x \n",vl_reg);
vl_reg &= ~(7 << ( gpio_nr % 10) * 3);
vl_reg |= ((func & 0x7) << (( gpio_nr % 10) * 3));
LDBG("GPFSEL VALUE AFTER_me %x \n",vl_reg);
iowrite32( vl_reg,(void *)vl_address);
vl_reg = ioread32((void *)vl_address);
LDBG("GPFSEL VALUE AFTER %x \n",vl_reg);
}

void GpioSetState(void __iomem * vl_gpio_base , uint32_t gpio_nr , uint32_t state){
uint32_t vl_address;
uint32_t vl_reg = 0;

if(state){
	vl_address = (uint32_t)vl_gpio_base + (uint32_t)IO_GPIO_GPSET0_OF;
}else{
	vl_address = (uint32_t)vl_gpio_base + (uint32_t)IO_GPIO_GPCLR0_OF;
}

vl_address += ((gpio_nr / 32 ) << 2);

LDBG("GPxxx ADDRESS %x \n",vl_address);
vl_reg |= (1 << ( gpio_nr % 32));
LDBG("GPxxx VALUE AFTER_me %x \n",vl_reg);
iowrite32( vl_reg,(void *)vl_address);

}


void ReportGpioHwValues( char *buf ,int * len ,void __iomem * vl_gpio_base ){

*len += sprintf(buf + *len,"GPFSEL0 = %x\n",ioread32(vl_gpio_base + IO_GPIO_GPFSEL0_OF));
*len += sprintf(buf + *len,"GPFSEL1 = %x\n",ioread32(vl_gpio_base + IO_GPIO_GPFSEL1_OF));
*len += sprintf(buf + *len,"GPFSEL2 = %x\n",ioread32(vl_gpio_base + IO_GPIO_GPFSEL2_OF));
*len += sprintf(buf + *len,"GPFSEL3 = %x\n",ioread32(vl_gpio_base + IO_GPIO_GPFSEL3_OF));
*len += sprintf(buf + *len,"GPFSEL4 = %x\n",ioread32(vl_gpio_base + IO_GPIO_GPFSEL4_OF));
*len += sprintf(buf + *len,"GPFSEL5 = %x\n",ioread32(vl_gpio_base + IO_GPIO_GPFSEL5_OF));


}


