/**
RADU DEV ADC 
*/
#include <asm/io.h>
#include <linux/module.h>
#include <linux/types.h> 
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/workqueue.h>
#include <linux/sched.h> /*scheduler related imported for current pointer*/
#include <linux/spinlock.h>


#include "radu_dev_gpio_LLD.h"
#include "radu_dev_spi_LLD.h"
#include "radu_dev_adc_LLD.h"
#include "radu_dev.h"
#include "radu_dev_debug.h"

#define ADC_CH_MASK 0x07
#define ADC_SGL 0x08
#define ADC_START 0x10

adc_buff_t adc_buff;

/*USED TO PUTTING TO NANI THE PORCESS THAT STARTED THE CONVERSION*/
DECLARE_WAIT_QUEUE_HEAD(adc_wait);

irqreturn_t SpiAdcIrq(int irq, void *dev_id){

ALDBG( "Irq!%d\n",irq);
//ALDBG( "I=%d\n",adc_buff.index);
switch(adc_buff.index)
{
	case 0:
		GpioSetState( p_gpio_base , SPI_SPI0_CE0, 0u);
		
		SpiSendIrq( p_spi_base ,adc_buff.data[0], 0);
		adc_buff.index++;
		break;
	case 1:
		SpiSendIrq( p_spi_base ,adc_buff.data[1], 1);
		adc_buff.index++;
		break;
	case 2:
		SpiSendIrq( p_spi_base ,adc_buff.data[2], 1);
		adc_buff.index++;
		break;
	case 3:
		GpioSetState( p_gpio_base , SPI_SPI0_CE0, 1u);
		adc_buff.index=0;
		
		/*READ ADC RESULT*/
		{
		uint16_t temp = 0;
		SpiRead(p_spi_base); // dummy read only HIGH Z
		temp  = SpiRead(p_spi_base);
		temp &= (uint32_t)0x7f; // ERASE THE VALUE THAT IS IN HIGH Z
		temp = temp << 8;
		temp |= SpiRead(p_spi_base);
		temp = temp >> 4; //ONLY four matter!
		atomic_set(&adc_buff.last_value, (int)temp);
		ALDBG( "Value=%08x\n",temp);
		}
		

		/*finish the aqsition if some ioctl operatio is requested*/
		if(atomic_read(&adc_buff.atomic_flag) == 0 ){
			
			SpiClearIrq(p_spi_base); // disable interrupts
			/*mean computed wake up the sleeping process*/
			atomic_set(&adc_buff.atomic_flag,1);
			wake_up_interruptible(&adc_wait); 
		}else{ /*start a new ADC conversion*/
			GpioSetState( p_gpio_base , SPI_SPI0_CE0, 0u);
			
			SpiSendIrq( p_spi_base ,adc_buff.data[0], 0);
			adc_buff.index++;
		}
		
		
		break;
}

//ALDBG( "F=%d\n",adc_buff.flag);
return IRQ_HANDLED;
}
int AdcInit(uint16_t * buffer_add){
  GpioSetFunction(p_gpio_base , SPI_SPI0_MOSI , GPIO_ALT0);
  GpioSetFunction(p_gpio_base , SPI_SPI0_MISO , GPIO_ALT0);
  GpioSetFunction(p_gpio_base , SPI_SPI0_SCLK , GPIO_ALT0);
  GpioSetFunction(p_gpio_base , SPI_SPI0_CE0 , GPIO_OUTPUT); 
  GpioSetState( p_gpio_base , SPI_SPI0_CE0, 1u);
  SpiInitFunction(p_spi_base, 65536);
  adc_buff.data[1] =0xAA;// for debug porpuse to see the buss on oscilloscope
  adc_buff.data[2] =0xAA;// for debug porpuse to see the buss on oscilloscope
  adc_buff.index=0;
  adc_buff.med_value=0;
  adc_buff.read_buffer = buffer_add;
  
  atomic_set(&adc_buff.atomic_flag,0);
  
  adc_buff.buff_index=0;
  adc_buff.channel =0;
  return 0;
}

void AdcSetCh(uint16_t channel){
adc_buff.channel = channel;
adc_buff.data[0]=(adc_buff.channel & ADC_CH_MASK) | ADC_SGL | ADC_START;
}

uint32_t AdcStartSampling(void){

adc_buff.data[0]=(adc_buff.channel & ADC_CH_MASK) | ADC_SGL | ADC_START;
atomic_set(&adc_buff.atomic_flag,1);
	
SpiStartIrq( p_spi_base , 0);
 
return 0;
}

int AdcDeinit(void){
  GpioSetFunction(p_gpio_base , SPI_SPI0_MOSI , GPIO_INPUT);
  GpioSetFunction(p_gpio_base , SPI_SPI0_MISO , GPIO_INPUT);
  GpioSetFunction(p_gpio_base , SPI_SPI0_SCLK , GPIO_INPUT);
  GpioSetFunction(p_gpio_base , SPI_SPI0_CE0 , GPIO_INPUT);
  SpiDeinitFunction(p_spi_base);
   return 0;
}








