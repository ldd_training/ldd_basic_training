#!/usr/bin/python
import time
import fcntl #This module performs file control and I/O control on file descriptors


# Open a file
fo = open("/dev/my_adc", "r",0);


fcntl.ioctl(fo,1,0);
result=fo.read(9);
print  " result= " + result +"\n"
time.sleep(0.1);
result=fo.read(9);
print  " result= " + result +"\n"
fcntl.ioctl(fo,2,0);
time.sleep(0.1);
result=fo.read(9);
print  " result= " + result +"\n"

time.sleep(1);

fcntl.ioctl(fo, 0, 0);

fo.close();
