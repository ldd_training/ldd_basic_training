
#ifndef RADU_DEV_ADC
#define RADU_DEV_ADC
#include <asm/atomic.h>

#define ADC_BUF_SIZE 2000
#define ADC_MED_SIZE 1990

typedef struct{
		 uint8_t data[3];
		 uint8_t index;
         uint16_t buff_index;
		 uint16_t * read_buffer;
		 uint16_t med_value;
		 atomic_t atomic_flag;
		 uint16_t channel;
		 atomic_t last_value;
} adc_buff_t;

extern adc_buff_t adc_buff;
extern spinlock_t adc_lock;
extern spinlock_t ioctl_lock;
extern wait_queue_head_t adc_wait;

  int AdcInit(uint16_t * buffer_add);
  void AdcSetCh(uint16_t channel);
  uint32_t AdcStartSampling(void);
  int AdcDeinit(void);
extern irqreturn_t SpiAdcIrq(int irq, void *dev_id);

#endif