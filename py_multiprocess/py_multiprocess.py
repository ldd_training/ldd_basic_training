#!/usr/bin/env python

"""A basic fork in action"""

import os
import fcntl

def my_fork():
    child_pid = os.fork()

    # Open a file
    fo = open("/dev/my_adc", "w",0);
    if child_pid == 0:
        print "Child Process: PID# %s" % os.getpid()
		fcntl.ioctl(fo, 0, 0);
        result=fo.read();
		print  "Child result %d" % result
    else:
        print "Parent Process: PID# %s" % os.getpid()
		fcntl.ioctl(fo, 0, 0);
        result=fo.read();
		print  "Parent result %d" % result

if __name__ == "__main__":
    my_fork()
