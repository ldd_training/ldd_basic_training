
#ifndef RADU_DEV_ADC
#define RADU_DEV_ADC

#define ADC_BUF_SIZE 2000
#define ADC_MED_SIZE 1990

typedef struct{
		 uint8_t data[3];
		 uint8_t index;
         uint16_t buff_index;
		 uint16_t * read_buffer;
		 uint16_t med_value;
volatile uint16_t flag;
		 uint16_t channel;
} adc_buff_t;

extern wait_queue_head_t adc_wait;
extern adc_buff_t adc_buff;



  int AdcInit(uint16_t * buffer_add);
  void AdcSetCh(uint16_t channel);
  uint32_t AdcChRead(void);
  int AdcDeinit(void);
extern irqreturn_t SpiAdcIrq(int irq, void *dev_id);

#endif