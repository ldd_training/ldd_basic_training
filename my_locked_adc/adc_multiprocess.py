#!/usr/bin/env python



import os
import fcntl

def my_fork():
    child_pid = os.fork()

    # Open a file
    fo = open("/dev/my_adc", "r",0);
    if child_pid == 0:
        print "ENTER Child Process: PID# %s \n" % os.getpid()
	fcntl.ioctl(fo,6, 0);
        result=fo.read(9);
	print  "Child result= " + result +"\n"
    else:
        print "ENTER Parent Process: PID# %s\n" % os.getpid()
	fcntl.ioctl(fo, 0, 0);
        result=fo.read(9);
	print  "Parent result= " + result +"\n"

if __name__ == "__main__":
    my_fork()
