
#ifndef RADU_PROCESS_DATA_H
#define RADU_PROCESS_DATA_H


void DataProcess(unsigned long data_pointer);

void DataProcessW(struct work_struct *work);

typedef struct {
  struct work_struct work;
  uint32_t    data;
} adc_work_t;

extern struct tasklet_struct process_tasklet;
extern struct workqueue_struct  * p_my_workqueue;
extern  adc_work_t adc_work;

#endif