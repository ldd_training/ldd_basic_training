/**
RADU DEV GPIO LLD
*/
#include <asm/io.h>
#include <linux/module.h>
#include <linux/types.h> 
#include <linux/kernel.h>
#include "radu_dev_spi_LLD.h"
#include "radu_dev_debug.h"


/**    SpiInitFunction
*/
void SpiInitFunction(volatile uint32_t __iomem * vl_spi_base, uint32_t vl_clk){
volatile uint32_t * vl_cs_address;
volatile uint32_t * vl_clk_address;
volatile uint32_t * vl_fifo_address;
uint32_t vl_dummy;
int i;

vl_cs_address = (uint32_t *)((uint32_t)vl_spi_base + (uint32_t)SPI_CS_OF);
vl_clk_address = (uint32_t *)((uint32_t)vl_spi_base + (uint32_t)SPI_CLK_OF);
vl_fifo_address = (uint32_t *)((uint32_t)vl_spi_base + (uint32_t)SPI_FIFO_OF);

SLDBG("SPI_CS ADD %08x\n",vl_cs_address);
SLDBG("SPI_CLK ADD %08x\n",vl_clk_address);
*vl_cs_address =0;
*vl_cs_address = SPI_CS_CS(0) | SPI_CS_CLEAR(0x3);
*vl_clk_address = vl_clk;
for(i=0;i<SPI_FIFO_SIZE;i++){
vl_dummy = *vl_fifo_address;
}

}
/**    SpiDeinitFunction
*/
void SpiDeinitFunction(volatile uint32_t __iomem * vl_spi_base){
volatile uint32_t * vl_cs_address;
volatile uint32_t * vl_clk_address;
vl_cs_address = (uint32_t *)((uint32_t)vl_spi_base + (uint32_t)SPI_CS_OF);
vl_clk_address = (uint32_t *)((uint32_t)vl_spi_base + (uint32_t)SPI_CLK_OF);
*vl_cs_address =0;
*vl_clk_address = 0;
}
/**    SpiSend
*/
void SpiSend( volatile uint32_t __iomem * vl_spi_base , uint32_t vl_data,uint32_t vl_clk_pol){
volatile uint32_t * vl_cs_address;
volatile uint32_t * vl_fifo_address;
uint32_t vl_reg_value =0;

vl_cs_address = (uint32_t *)((uint32_t)vl_spi_base + (uint32_t)SPI_CS_OF);
vl_fifo_address = (uint32_t *)((uint32_t)vl_spi_base + (uint32_t)SPI_FIFO_OF);

if(vl_clk_pol){
vl_reg_value |= SPI_CS_CPHA; 
}
vl_reg_value |= SPI_CS_TA;
SLDBG(" SPI CS value %08x\n",*vl_cs_address);

*vl_cs_address = vl_reg_value;
*vl_fifo_address = vl_data;

while((*vl_cs_address & SPI_CS_DONE) == 0)
{
SLDBG(" waining... %08x\n",*vl_cs_address);
}
*vl_cs_address &=~SPI_CS_TA;
}
/**    SpiSendIrq
*/
void SpiSendIrq( volatile uint32_t __iomem * vl_spi_base , uint32_t vl_data,uint32_t vl_clk_pol){
volatile uint32_t * vl_cs_address;
volatile uint32_t * vl_fifo_address;
uint32_t vl_reg_value =0;

vl_cs_address = (uint32_t *)((uint32_t)vl_spi_base + (uint32_t)SPI_CS_OF);
vl_fifo_address = (uint32_t *)((uint32_t)vl_spi_base + (uint32_t)SPI_FIFO_OF);

if(vl_clk_pol){
vl_reg_value |= SPI_CS_CPHA; 
}
vl_reg_value |= SPI_CS_TA | SPI_CS_INTD;
SLDBG("  SpiSendIrq->CS %08x\n",*vl_cs_address);

*vl_fifo_address = vl_data;
*vl_cs_address = vl_reg_value;

}

void SpiStartIrq( volatile uint32_t __iomem * vl_spi_base , uint32_t vl_clk_pol){
volatile uint32_t * vl_cs_address;
uint32_t vl_reg_value =0;

vl_cs_address = (uint32_t *)((uint32_t)vl_spi_base + (uint32_t)SPI_CS_OF);

/*clear FIFOs*/
*vl_cs_address = SPI_CS_CLEAR(1);
*vl_cs_address = SPI_CS_CLEAR(2);
SLDBG(" SpiStartIrq->CS %08x\n",*vl_cs_address);

if(vl_clk_pol){
vl_reg_value |= SPI_CS_CPHA; 
}
vl_reg_value |= SPI_CS_TA | SPI_CS_INTD;
*vl_cs_address = vl_reg_value;

}
/**    SpiClearIrq
*/
void  SpiClearIrq(volatile uint32_t __iomem * vl_spi_base){
volatile uint32_t * vl_cs_address;
vl_cs_address = (uint32_t *)((uint32_t)vl_spi_base + (uint32_t)SPI_CS_OF);
*vl_cs_address &= ~(SPI_CS_TA | SPI_CS_INTD);
}
/**    SpiSetIrq
*/
 void  SpiSetIrq(volatile uint32_t __iomem * vl_spi_base){
volatile uint32_t * vl_cs_address;
vl_cs_address = (uint32_t *)((uint32_t)vl_spi_base + (uint32_t)SPI_CS_OF);
*vl_cs_address |= SPI_CS_INTD;
}
/**    SpiClkPol
*/
void  SpiClkPol(volatile uint32_t __iomem * vl_spi_base,uint32_t vl_clk_pol){
volatile uint32_t * vl_cs_address;
vl_cs_address = (uint32_t *)((uint32_t)vl_spi_base + (uint32_t)SPI_CS_OF);
if(vl_clk_pol){
*vl_cs_address |= SPI_CS_CPHA; 
}
}
/**    SpiRead
*/
uint32_t SpiRead( volatile uint32_t __iomem * vl_spi_base){
volatile uint32_t * vl_fifo_address;

vl_fifo_address = (uint32_t *)((uint32_t)vl_spi_base + (uint32_t)SPI_FIFO_OF);

return *vl_fifo_address;

}





