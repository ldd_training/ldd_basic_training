/* ofd.c � Our First Driver code */
#include <linux/module.h> /*module_init() and module_exit(), which are defined in the kernel header module.h.*/
#include <linux/version.h> /*Additionally, version.h is included for the module version to be compatible with the kernel*/
#include <linux/kernel.h>/*Note that there is no stdio.h (a user-space header); instead, we use the analogous kernel.h (a kernel space header). printk() is the equivalent of printf().*/
#include <linux/types.h> /*dev_t defined in kernel header linux/types.h*/
#include <linux/kdev_t.h> /*MAJOR MINOR MKDEV Macros (defined in kernel header linux/kdev_t.h)*/
#include <linux/fs.h> /*alloc_chrdev_region(dev_t *first, unsigned int firstminor, unsigned int cnt, char *name);*/
#include <linux/device.h>/* driver is concerned, the appropriate /sys entries need to be populated using the Linux device model APIs declared in <linux/device.h>.*/
#include <linux/cdev.h>/*oth cdev_init() and cdev_add() are declared in <linux/cdev.h>*/
#include <asm-generic/uaccess.h>/*copy_to_user() and copy_from_user*/

#include <asm/io.h>
#include <linux/proc_fs.h> /*All modules that work with /proc should include*/
#include <linux/timer.h>/*kernel timmers*/
#include <linux/jiffies.h>/*the time base 10ms on pi*/
#include <linux/slab.h> /*kmalloc*/
//#include <asm-generic/page.h>
#include <linux/string.h>
#include <linux/interrupt.h>
#include <linux/moduleparam.h>
#include <linux/stat.h> /*module_param field is a permission value;*/
#include <linux/workqueue.h>
#include <linux/sched.h> /*scheduler related imported for current pointer*/
#include <linux/mutex.h>


#include "radu_process_data.h"
#include "radu_dev_adc_LLD.h"
#include "radu_dev_gpio_LLD.h"
#include "radu_dev_spi_LLD.h"
//#include "radu_dev.h"


/*DRIVER INTERNAL DEFINES*/
#define SPI_IRQ 80
#define DEV_NAME "my_adc"
#define PAGES_ALOCATED  0 // actually 2^PAGES_ALOCATED
/*DRIVER INTERNAL DEFINES END*/
/*DRIVER TYPE DEFINES*/

/*DRIVER DEFINES END*/
/*DRIVER INTERNAL GLOBAL VARIABLES*/

struct my_adc_drv {
	dev_t radu_dev; // Global variable for device driver (major minor)
	struct cdev c_dev; // Global variable for the character device structure
	struct class * dev_class;  // Global variable for the device class
	unsigned long v_data;
	struct mutex  adc_lock;
	
} drv_data;

volatile uint32_t __iomem * p_gpio_base = NULL;
volatile uint32_t __iomem * p_spi_base = NULL;



/* return 0 == succes*/
static int radu_open(struct inode *i, struct file *f)
{
int error =0;
	printk(KERN_INFO "char Driver: open() PID:%d\n", current->pid);
	if(mutex_lock_interruptible((struct mutex *)&drv_data.adc_lock)){
		printk(KERN_INFO "Sleeping PID:%d interrupted \n", current->pid);
		return -1;
	}
	if((error=request_irq(SPI_IRQ, SpiAdcIrq, 0, DEV_NAME ,(void *)&drv_data)) != 0){
		printk(KERN_INFO "BUBA irq request error %d\n",error);
		return -1;
	}

	adc_work.data = 666; //dummy just to see that this value cand be used in WorkQ context
	INIT_WORK( (struct work_struct *)&adc_work, DataProcessW );
  return 0;
}
/* return 0 == succes*/
static int radu_close(struct inode *i, struct file *f)
{
 
  free_irq(SPI_IRQ, (void *)&drv_data);
  flush_workqueue(p_my_workqueue); /*flush the particular work queue (which blocks until the handler has completed processing of the work)*/
	
  printk(KERN_INFO "char Driver: close() PID:%d\n", current->pid);
  mutex_unlock((struct mutex *)&drv_data.adc_lock);
  return 0;
}
/*returning a negative number would be a usual error*/
/*return bytes read*/
static ssize_t radu_read(struct file *f, char __user *buf, size_t count, loff_t *possition)
{ 
	char char_result[10];
	uint16_t vl_result ,len=0;
	printk(KERN_INFO "char Driver: read() PID:%d\n", current->pid);
/*	
	if(*possition > 0){
	*possition = 0;
	return 0;
	}
*/	
	vl_result = AdcChRead();
	
	len = sprintf( &char_result[0],"%08x\n",vl_result);
	
		if (copy_to_user(buf, &char_result[0],len ) != 0){
			return -EFAULT;
		}
		else {
			*possition += len;
			return count;
		}
     
}


/*returning a negative number would be a usual error*/
/*return bytes written*/
/*message format 1 char ticker  2 char gpio nr  1 char on/off 3 char delay ms*10 */
 static ssize_t radu_write(struct file *f, const char __user *buf, size_t count, loff_t *possition)
{
  char c[10];
  uint32_t vl_chan_nr = 0;
  uint32_t vl_result = 0;
  
  printk(KERN_INFO " char Driver: write()\n");
	if (copy_from_user(&c, buf, (count > 10 ? 10 : count)) != 0){
		return -EFAULT;
	}
	else
	{	
	vl_chan_nr = c[0]-48;
	 printk(KERN_INFO " chan_nr = %d\n",vl_chan_nr);
	
	printk(KERN_INFO " CH_%d = %08x \n",vl_chan_nr,vl_result);	
		/*fops related*/
		possition += count;
		return count;
	}
	
}


/*The ioctl system call offers a way to issue device-specific commands*/
static long radu_ioctl(struct file *fo, unsigned int comm , unsigned long arg){

printk(KERN_INFO"IOCTL CHANNEL:%d PID :%d\n",comm,current->pid);

AdcSetCh((uint16_t)comm);

return 0;
}
/* fill in a file operations structure with the desired file operations*/
static struct file_operations radu_fops =
{
  .owner = THIS_MODULE,
  .open = radu_open,
  .release = radu_close,
  .read = radu_read,
  .write = radu_write,
  .unlocked_ioctl = radu_ioctl
};

/*PROC INTERFACE*/
int radu_read_procmem(char *buf, char **start, off_t offset, int count, int *eof, void *data){
	int len = 0;
	ReportGpioHwValues( buf ,&len,p_gpio_base);

	len += sprintf(buf + len,"L%d\n",len);
	*eof = 1;
	return len;
}

/*PROC INTERFACE END */
static int __init radu_init(void) /* Constructor */
{
	//init mutex
	mutex_init((struct mutex *)&drv_data.adc_lock);
	/*Like vmalloc, ioremap builds new page tables; unlike vmalloc, however, it doesn�t
	 actually allocate any memory. The return value of ioremap is a special virtual address
     that can be used to access the specified physical address range*/ 
	if ((p_spi_base = ioremap(SPI_BASE_ADD , SPI_SIZE)) == NULL){
        printk(KERN_ERR "Mapping SPI failed\n");
		goto fault_0;
    }
	
	/*Like vmalloc, ioremap builds new page tables; unlike vmalloc, however, it doesn�t
	 actually allocate any memory. The return value of ioremap is a special virtual address
     that can be used to access the specified physical address range*/ 
	if ((p_gpio_base = ioremap(IO_GPIO_BASE , IO_GPIO_SIZE)) == NULL){
        printk(KERN_ERR "Mapping GPIO failed\n");
		goto fault_1_2;
    }
	
	/*The second API dynamically figures out a free major number, and registers the cnt number
		of device file numbers starting from <the free major, firstminor>,*/
    if(alloc_chrdev_region(&drv_data.radu_dev , 0 ,1,"my_adc") < 0){
		printk(KERN_ERR "Char driver region alocation failed\n");
		goto fault_1;
    }
	/*/sys entries need to be populated using the Linux device model APIs declared*/
	/*The rest should be handled by udev*/
	if ((drv_data.dev_class = class_create(THIS_MODULE, "chardrv")) == NULL){	
		printk(KERN_ERR "Device class failed\n");
		goto fault_2;
	}
	/*Then, the device info (<major, minor>) under this class is populated by:*/
	if (device_create(drv_data.dev_class, NULL, drv_data.radu_dev, NULL, DEV_NAME) == NULL){
		printk(KERN_ERR "Device create failed\n");
		goto fault_3;
	}
	/*initialise the character device structure*/
	cdev_init(&drv_data.c_dev, &radu_fops);
	/* hand this structure to the VFS using the call cdev_add().*/
	if (cdev_add(&drv_data.c_dev, drv_data.radu_dev, 1) == -1){
		goto fault_4;
	}
	if(create_proc_read_entry("my_adc_stat", 0 /* default mode */,NULL /* parent dir */
	, radu_read_procmem, NULL /* client data */) == NULL){
		goto fault_4;
	}	
	if((drv_data.v_data = __get_free_pages(GFP_KERNEL |__GFP_DMA, PAGES_ALOCATED)) == 0){
		printk(KERN_INFO "__get_free_pages FAILED\n");
		goto fault_5;
	}
		/*GFP_KERNEL-Normal allocation of kernel memory. May sleep.*/
	/*__GFP_DMA This flag requests allocation to happen in the DMA-capable memory zone.*/
	/*__GFP_COLD requests a �cold� page, which has not been used in some time.*/
	/*if((v_data = (unsigned long)kmalloc(4096, GFP_KERNEL |__GFP_DMA|__GFP_COLD)) == 0){
			printk(KERN_INFO "kmalloc FAILED\n");
		goto fault_5;
	}*/else{
	memset((void *)drv_data.v_data,0xAA,4096);/*fill with AA for debuging porpuse*/
	if((p_my_workqueue =(struct workqueue_struct *)create_singlethread_workqueue("adc_work")) == 0){
		printk(KERN_INFO "BUBA workq request error \n");
		return -1;
	}

	}
	
	AdcInit((uint16_t *)drv_data.v_data);

	printk(KERN_INFO "++++++++++++++++++++++++\n");
	printk(KERN_INFO "my_adc device registered\n");
	printk(KERN_INFO "MAJOR=%d , MINOR=%d\n",MAJOR(drv_data.radu_dev),MINOR(drv_data.radu_dev));
	printk(KERN_INFO "page_add=0x%08x\n",(uint32_t)drv_data.v_data);
	printk(KERN_INFO "gpio_add=0x%08x\n",(uint32_t)p_gpio_base);
	printk(KERN_INFO "spi_add=0x%08x\n",(uint32_t)p_spi_base);
	printk(KERN_INFO "++++++++++++++++++++++++\n");

    return 0;
	
	fault_5 : remove_proc_entry("my_adc_stat", NULL /* parent dir */);
	fault_4 : device_destroy(drv_data.dev_class, drv_data.radu_dev);
	fault_3 : class_destroy(drv_data.dev_class);
	fault_2 : unregister_chrdev_region(drv_data.radu_dev, 1);
	fault_1 : iounmap(p_gpio_base);
	fault_1_2 :iounmap(p_spi_base);
	fault_0 : 
	
	return -1;
}
static void __exit radu_exit(void) /* Destructor */
{

	AdcDeinit();
	 destroy_workqueue(p_my_workqueue);
	free_pages(drv_data.v_data, PAGES_ALOCATED);
	/*kfree((void *)v_data);*/
	remove_proc_entry("my_adc_stat", NULL /* parent dir */);
	device_destroy(drv_data.dev_class, drv_data.radu_dev);
    class_destroy(drv_data.dev_class);
	unregister_chrdev_region(drv_data.radu_dev, 1);
	iounmap(p_gpio_base);
	iounmap(p_spi_base);
	
	printk(KERN_INFO "++++++++++++++++++++++++\n");
    printk(KERN_INFO "my_adc device unregistered\n");
	printk(KERN_INFO "++++++++++++++++++++++++\n");

}
 
module_init(radu_init);
module_exit(radu_exit);
 
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Radu Radu");
MODULE_DESCRIPTION("Adc driver");
