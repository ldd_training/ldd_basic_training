#include <linux/types.h> 
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/workqueue.h>
#include <linux/sched.h> /*scheduler related imported for current pointer*/
#include <linux/wait.h> /*uset at sleeping*/

#include "radu_dev_adc_LLD.h"
#include "radu_dev_debug.h"
#include "radu_process_data.h"



/*When asynchronous execution context is needed, a work item
describing which function to execute is put on a queue. 
An independent thread serves as the asynchronous execution context.The
queue is called workqueue and the thread is called worker
While there are work items on the workqueue the worker executes the
functions associated with the work items one after the other.  When
there is no work item left on the workqueue the worker becomes idle.
When a new work item gets queued, the worker begins executing again.*/
struct workqueue_struct  * p_my_workqueue;

/*A work item is a simple struct that holds a pointer to the function
that is to be executed asynchronously.*/

adc_work_t adc_work;


DECLARE_TASKLET(process_tasklet, DataProcess, (unsigned long)&adc_buff);

void DataProcess(unsigned long data_pointer){

uint16_t * pl_adc_buff = ((adc_buff_t *)data_pointer)->read_buffer;
uint32_t   med_val = 0;
int i,j;

PDBG("TASKLET enter PID:%d\n", current->pid);

for(j=0 ; j < 100 ;j++){ /*Dummy for loop just to make the tasklet last longer*/
	med_val = 0;

	for(i=0; i < ADC_MED_SIZE ; i++){
		med_val += pl_adc_buff[i];
	}
	med_val /= ADC_MED_SIZE; 

}
adc_buff.med_value = (uint16_t)med_val;
adc_buff.flag=1;
/*mean computed wake up the sleeping process*/
wake_up_interruptible(&adc_wait);

PDBG("TASKLET exit med value =%08x \n",med_val);

}

void DataProcessW(struct work_struct *work){

adc_work_t * adc_w_struct =(adc_work_t *)work;

uint32_t med_val = 0;
int i,j;


PDBG("WORKq enter PID:%d\n", current->pid);
//PDBG("DATA  %08x\n",adc_w_struct->data);
for(j=0 ; j < 1000 ;j++){ /*Dummy for loop just to make the work last longer*/
	med_val = 0;

	for(i=0; i < ADC_MED_SIZE ; i++){
		med_val += i/(j+1); /*Dummy computation*/
	}
	med_val /= ADC_MED_SIZE; 
}

PDBG("WORKq exit PID:%d\n", current->pid);

}



