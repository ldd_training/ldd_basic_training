


#ifndef RADU_DEV_LLD
#define RADU_DEV_LLD
/*HW LOW LEVEL ADDRESSES*/
#define IO_GPIO_BASE 0x20200000
#define IO_GPIO_SIZE 0xFFF
#define IO_GPIO_GPFSEL0_OF (uint32_t)0x00
#define IO_GPIO_GPFSEL1_OF (uint32_t)0x04 
#define IO_GPIO_GPFSEL2_OF (uint32_t)0x08 
#define IO_GPIO_GPFSEL3_OF (uint32_t)0x0C 
#define IO_GPIO_GPFSEL4_OF (uint32_t)0x10
#define IO_GPIO_GPFSEL5_OF (uint32_t)0x14

#define IO_GPIO_GPSET0_OF (uint32_t)0x1C
#define IO_GPIO_GPSET1_OF (uint32_t)0x20 

#define IO_GPIO_GPCLR0_OF (uint32_t)0x28
#define IO_GPIO_GPCLR1_OF (uint32_t)0x2C   
 
/*HW LOW LEVEL ADDRESSES END */

#define GPIO_INPUT 0x00
#define GPIO_OUTPUT 0x01
#define GPIO_ALT0 0x4
#define GPIO_ALT1 0x5
#define GPIO_ALT2 0x6
#define GPIO_ALT3 0x7
#define GPIO_ALT4 0x3
#define GPIO_ALT5 0x2


extern void ReportGpioHwValues( char *buf ,int * len ,void __iomem * vl_gpio_base );
extern void GpioSetFunction(volatile uint32_t __iomem * vl_gpio_base , uint32_t gpio_nr , uint32_t func);
extern void GpioSetState( volatile uint32_t __iomem * vl_gpio_base , uint32_t gpio_nr , uint32_t state);
#endif