
#ifndef RADU_DEV_SPI_LLD
#define RADU_DEV_SPI_LLD
/*HW LOW LEVEL ADDRESSES*/
//#define SPI_BASE_ADD 0x20204000
#define SPI_BASE_ADD   0x20204000

#define SPI_SIZE 0xFF
#define SPI_CS_OF (uint32_t)0x00
#define SPI_FIFO_OF (uint32_t)0x04 
#define SPI_CLK_OF (uint32_t)0x08 
#define SPI_DLEN_OF (uint32_t)0x0C 
#define SPI_LTOH_OF (uint32_t)0x10
#define SPI_DC_OF (uint32_t)0x14

#define SPI_FIFO_SIZE (uint32_t)16

/*SPI REGS_VALUES*/

#define SPI_CS_CSPOL1 (uint32_t)(1 << 22)
#define SPI_CS_CSPOL0 (uint32_t)(1 << 21)
#define SPI_CS_TXD (uint32_t)( 1 << 18)
#define SPI_CS_RXD (uint32_t)( 1 << 17)
#define SPI_CS_DONE (uint32_t)( 1 << 16)
#define SPI_CS_ADCS (uint32_t)( 1 << 11)
#define SPI_CS_INTR (uint32_t)( 1 << 10)
#define SPI_CS_INTD (uint32_t)(1 << 9)
#define SPI_CS_DMAEN (uint32_t)(1 << 8)
#define SPI_CS_TA   (uint32_t)(1 << 7)
#define SPI_CS_CSPOL (uint32_t)(1 << 6)
#define SPI_CS_CLEAR(fifo) (uint32_t)((fifo & 0x03) << 4)
#define SPI_CS_CPOL (uint32_t)(1 << 3)
#define SPI_CS_CPHA (uint32_t)(1 << 2)
#define SPI_CS_CS(cs) (uint32_t)( (cs & 0x03) << 0)

/*SPI PINS*/
#define SPI_SPI0_MOSI (uint32_t)10
#define SPI_SPI0_MISO (uint32_t)9
#define SPI_SPI0_SCLK (uint32_t)11
#define SPI_SPI0_CE0 (uint32_t)8
 


extern void SpiInitFunction(volatile uint32_t __iomem * vl_spi_base, uint32_t vl_clk);
extern void SpiDeinitFunction(volatile uint32_t __iomem * vl_spi_base);
extern void SpiSend( volatile uint32_t __iomem * vl_spi_base , uint32_t vl_data,uint32_t vl_clk_pol);
extern void SpiSendIrq( volatile uint32_t __iomem * vl_spi_base , uint32_t vl_data,uint32_t vl_clk_pol);
extern void SpiStartIrq( volatile uint32_t __iomem * vl_spi_base , uint32_t vl_clk_pol);
extern uint32_t SpiRead( volatile uint32_t __iomem * vl_spi_base);
extern  void  SpiSetIrq(volatile uint32_t __iomem * vl_spi_base);
extern  void  SpiClearIrq(volatile uint32_t __iomem * vl_spi_base);
extern void  SpiClkPol(volatile uint32_t __iomem * vl_spi_base,uint32_t vl_clk_pol);
#endif